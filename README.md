# Line_selections

The script contains the code that reads full spectra and elemental spectra, identifies the lines, characterises them, then stores them in a pandas DataFrame. 



## Name
select_lines.py

## Description
Algorithm and examples shown in Kordopatis, Hill, Lind (2023)

Steps of the Code: 
1) Identifies the centers of the lines by computing the first two derivatives of the element spectrum.  
    
2) Does a x-match with the vald linelist. 
When several VALD lines are within +/- 1 pixel from the derived line core, 
the line that has the highest value of the boltzmann equation is selected.
==>log(Boltzmann): log(N)=log(A) -(E_chi/kT)+log(gf)
    log(A) is a constant we can neglect
    loggf is in vald
    T is the temperature of the star
    E_chi is the excitation potential 
    
==> Caution: By using Boltzmann equation to select the lines,we assume that for a given element, 
all of the lines correspond to the same ionisation level. If this is not the case, 
we need to involve Saha's equation too. This is not implemented yet. 
==> Additional Caution: when there is hyperfine structure, then the lambda of Vald that we will 
find is not necessarily the center of the line we will be seeing

3) Estimates the depth of the line and compares it to Careyl's formula. sigma_fmin = 1.5/SNR_resol 
If the depth of the line is large enough to be seen at a given SNR, then the line is selected. 
    
    
4) We estimate the width of the line as the pixel in which the flux of the element itself is close enough to the continuum. 
    
Once the line is selected, we compute the ratio between the element spectrum and the full spectrum. 
Note: we require that if ratio<0.8 then we must have at least two pixels of the total spectrum with flux>0.9 within 1.5 FWHM,
    


## Input of the Code: 
* full_sp: DataFrame containing a column 'll' with the wavelengths and 'flux' with the full spectrum of a star (all the elements, all the molecules, blends etc)
* el_sp: DataFrame containing a column 'll' with the wavelengths and 'flux' with the spectrum of a given element only, computed in a similar way as full_sp (in order to have the same continuum opacities)
* Teff: Effective Teff, used in Boltzmann equation. 
* vald: pandas Dataframe containing the vald line-parameters (ll, Echi, loggf)
* purity_crit: minimum required purity to keep the line
* fwhm: in A. Resolution element of the spectrograph
* SNR : minimum SNR per resolution element (used for line detection)
* sampling: spectrum's sampling (in A)
* verbose (optional): print information while running

## Output of the Code:
pandas data frame, with the following columns:
* ll : central wavelength where either side of the line has a putiry higher than purity_crit
* Blueratio: Purity of the line, defined as the ratio of the element spectrum and the full spectrum at lambda0-1.5xFWHM. 
* Redratio : Purity of the line, defined as the ratio of the element spectrum and the full spectrum at lambda0+1.5xFWHM. 
* Fullratio: Purity of the line, defined as the ratio of the element spectrum and the full spectrum at lambda0+/-1.5xFWHM. 
* Maxratio:  max between the right and the left blend of the line.
* fmin: depth of the core of line (as identified by the algorithm) for the element spectrum
* fmin_sp: depth of the full spectrum at the position of the core of the line. 
* width: width in which the ratio has been computed
* BlueFlag: Number of pixels that have a flux>0.9 within 1.5 times the FWHM (resolution element) 
* RedFlag: Number of pixels that have a flux>0.9 within 1.5 times the FWHM (resolution element)

## Usage
        fwhm=0.350 # in units of 'll' in the spectrum
        min_purity=0.6
        snr=250
        sampling=0.05
        lines=select_lines(sp,el_sp, 5750, vald_linelist, min_purity, fwhm, snr, sampling, verbose=False)

## Support
Georges Kordopatis, Université Côte d'Azur, Observatoire de la Côte d'Azur, CNRS, Laboratoire Lagrange, nice, France
email: georges.kordopatis -at- oca.eu 


## Authors and acknowledgment
Authors: Georges Kordopatis, Vanessa Hill, Karin Lind

This work has benefited from several inspiring and fruitful discussions within the WEAVE and 4MOST consortia as well as  Michael Hanke. 
GK and VH gratefully acknowledge support from the french national research agency (ANR) funded project MWDisc (ANR-20-CE31-0004), as well as the Programme National Cosmology et Galaxies (PNCG) of CNRS/INSU with INP and IN2P3, co-funded by CEA and CNES.
KL acknowledges funds from the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (Grant agreement No. 852977) and funds from the Knut & Alice Wallenberg foundation.
